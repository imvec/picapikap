import time
import gpsd
import board
import digitalio
from time import sleep
from picamera import PiCamera
from datetime import datetime, timedelta
from adafruit_bme280 import basic as adafruit_bme280

spi = board.SPI()
bme_cs = digitalio.DigitalInOut(board.D5)
bme280 = adafruit_bme280.Adafruit_BME280_SPI(spi, bme_cs)

bme280.sea_level_pressure = 1013.25

gpsd.connect()

camera = PiCamera()
camera.resolution = (3280, 2464)

while True:
        tiempo = (time.strftime("%y%b%d_%H:%M:%S_"))
        packet = gpsd.get_current()
        tiempogps = str(packet.time)
        longitud = str("%0.5f_" % packet.lon)
        latitud = str("%0.5f_" % packet.lat)
        altitudgps = str("%0.1fm_" % packet.alt)
        temperatura = str("%0.1fC_" % bme280.temperature)
        humedad = str("%0.1f%%_" % bme280.relative_humidity)
        presion = str("%0.1fhPa" % bme280.pressure)
        altitud = str("%0.1fm_" % bme280.altitude)
        camera.capture("/home/pi/PicaPiKAP/media/" + tiempo + altitud + latitud + longitud + altitudgps + temperatura + humedad + presion + ".jpg")
        print("Datos del GPS")
        print("Satelites: " + str(packet.sats))
        print("Latitud:" + str("%0.5f" % packet.lat))
        print("Longitud:" + str("%0.5f" % packet.lon))
        print("Altitud GPS:" + str("%0.1f" % packet.alt))
        print("\nDatos del BME280")
        print("Temperatura: %0.1f C" % bme280.temperature)
        print("Humedad relativa: %0.1f %%" % bme280.relative_humidity)
        print("Presion: %0.1f hPa" % bme280.pressure)
        print("Altitud = %0.1f metros" % bme280.altitude)
        print("-----")
        time.sleep(3)
