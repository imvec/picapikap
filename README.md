# PicaPiKAP
Cámara estabilizada mecanicamente para la realización de fotografía aérea y la captura de datos ambientales utilizando cometas y globos cautivos.

### Un poco de contexto
En el I.M.V.E.C. empleamos la fotografía aerea para monitorizar espacios contaminados junto a las vecinas afectadas y para obtenerlas, utilizamos principalmente cometas en lugar de drones por diferentes motivos: rápido aprendizaje, reducido consumo energético, larguísimos tiempos de vuelo, no necesidad de licencia ni permiso de vuelo, equipación económica... Ante la necesidad de saber con exactitud a qué altura tomamos las fotografías, desde el I.M.V.E.C. decidimos en Mayo de 2022 iniciar el desarrollo de una cámara fotográfica abierta que junto a las fotografías de los espacios, también recogiese y añadiese a las imágenes la altitud y geolocalización a la que se dispararon. Durante el proceso, el equipo de desarrollo de herramientas del I.M.V.E.C. decidió añadir a la cámara la capacidad de obtener datos ambientales como temperatura humedad, presión, etc... dando a las fotografías valor añadido y permitiendo monitorizar los espacios investigados con mayor detalle.

El fruto de este proceso es el primer prototipo de la PicaPiKAP, una cámara basada en la minicomputadora Raspberry Pi, ensamblada junto a los sensore ambientales en un soporte estabilizado Picavet, un dispositivo tan antiguo como la fotografía, diseñado en el año 1912 por el inventor Pierre Picavet. 

**Pica de Picavet, Pi de Raspberry Pi y KAP de Kite Aerial Photography. Disfruten!**

# Lista de materiales
### Electrónica

- 1 RaspberryPi 3.
- 1 Tarjeta microSD clase 10 de 32gb.
- 1 Módulo de cámara Raspberry Pi V2.
- 1 [Módulo gps GT-U7](https://www.amazon.es/gp/product/B08GM3H878/ref=ppx_yo_dt_b_asin_title_o04_s00?ie=UTF8&psc=1).
- 1 [Sensor BME280 de Temperatura, humedad y presión atmosférica](https://www.amazon.es/gp/product/B072N8DJD5/ref=ppx_yo_dt_b_asin_title_o09_s00?ie=UTF8&psc=1).
- 1 botón de pulsación.
- 1 [Shield de Batería Lipo de 3800mAh](https://www.amazon.es/azdelivery-Portable-bater%C3%ADa-Power-Raspberry/dp/B072QZXTGS/ref=sr_1_2?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=1C6VCFMXJE83U&keywords=raspberry%2Bpi%2Bbattery&qid=1663359259&s=electronics&sprefix=raspberry%2Bpi%2Bbattery%2Celectronics%2C102&sr=1-2&th=1)
- 10 cables tipo jumper

_**Coste aproximado: +/- 130€**_

### Soporte Picavet

- 1 Caja de conexiónes de 150x100x7cm.
- 4 Cáncamos de métrica 5 (M5).
- 8 Tuercas autoblocantes.
- 10 Metros de cuerda de cometa.
- 2 Mosquetones.
- 1 Aro de plástico o metal ligero.
- 50 Centímetros de alambre de 3mm.

_**Coste aproximado: +/- 30€**_

#### Materiales para el montaje

- Taladro y brocas de 4 y 20mm.
- Cinta de doble lado o velcro adhesivo.
- Cuchilla.
- Llave inglesa.
- Alicate.

# Electrónica
El montaje consta de cinco elementos principales:
- 1 RaspberryPi 3.
- 1 Módulo de cámara Raspberry Pi V2.
- 1 Módulo gps GT-U7 con conexión UART.
- 1 Sensor BME280 (Temperatura, humedad y presión atmosférica) con conexión SPI.
- 1 Botón de pulsación.

Para ensamblar la parte electrónica de la PicaPiKAP sigue el siguiente esquema:

![](https://regist.ro/themes/registro/img/aereo/camaras/PicaPiKAP.png)

El botón sirve para apagar y encender la cámara. Si utilizas el shield de batería que enlazamos en la lista inicial de materiales probablemente no necesites este botón dado que consta de su propio interruptor de encendido y apagado pero en el caso que decidas alimentar la cámara con un powerbank de los de cargar el teléfono, este botón permitirá que apagues la cámara con un correcto cierre del sistema de archivos. Recuerda que la cámara es, en realidad, una computadora y el apagado debe realizarse correctamente para evitar la corrupción del sistema de archivos. 

# Software
La PicaPiKAP está costruída alrededor de la minicomputadora Raspberry Pi y dispone de un sistema operativo RaspberryOS, un port del sistema operativo Debian. Puedes descargar el sistema preconfigurado y listo para ser utilizado o instalar todos los elementos necesarios en una copia fresca del sistema RaspberryOS 32 bits utilizando el archivo "**instalador.sh**" de este repositorio. Si prefieres la segunda opción sigue las instrucciónes en la parte superior del archivo de instalación.

#### Sistema preconfigurado
Si tan solo quieres utilizar la cámara sin muchas complicaciones te recomendamos que descargues en el siguiente enlace nuestro sistema operativo preconfigurado y listo para utilizar. Tan solo tienes que descargar la imagen de disco en el siguiente enlace, flashearla regularmente como un sistema operativo para Raspberry Pi [usando Pi Imager](https://y.com.sb/watch?v=ntaXWS8Lk34) e introducirla en tu Raspberry Pi. La cámara comenazará a disparar 3 segundos después del arranque.

https://regist.ro/descargas/PicaPiKAP.tar.gz 

(md5sum:3ef87da2d23cdec2151c70795ba51826)

El sistema consta de dos particiones: "boot" y "rootfs". Tanto el programa de disparo como la carpeta de almacenamiento de las imágenes capturadas se encuentran en la partición "**rootfs**".

- El programa de disparo está ubicado en la ruta /home/pi/PicaPiKAP/scripts/PicaPiKAP.py.
- Las imágenes capturadas se almacenan en la ruta /home/pi/PicaPiKAP/media.

# Modo de empleo  
#### Configurar fecha y hora de la cámara
Dado que la Raspberry Pi no mantiene la fecha y la hora entre arranque y arranque y hemos prescindido de un módulo de reloj en tiempo real, la cámara obtiene la fecha y la hora conectándose a una red wifi. Para utilizar la PicaPiKAP debes cofigurar tu teléfono movil para crear un punto de acceso wifi con las siguientes credenciales y actívalo antes de utilizar la cámara:

- SSID: picapikap
- Password: AutodefensaVecinal

De este modo, al arrancar, la PicaPiKAP se conectará a la red y se pondrá en hora. Es irrelevante si tienes conexión a internet porque la sincronización se hace en relación a la fecha y hora del teléfono móvil.

#### Obtener imágenes 
Una vez que la cámara se haya conectado a la red wifi para obtener fecha y hora podemos dejar que la cámara se eleve en el aire. La mayoría de los teléfonos móviles permiten ver si hay dispositivos conectados a su punto de acceso. Deberías poder comprobar si hay un dispositivo conectado.

El programa de disparo (/home/pi/PicaPiKAP/scripts/PicaPiKAP.py) viene configurado para disparar cada tres segundos. Si deseas cambiar el intervalo de tiempo entre foto y foto debes cambiar el número presente en la última linea "time.sleep(3)". Cambia 3 por 5 si quieres que la cámara dispare cada 5 segundos.


#### Descargar las imágenes
Una vez que hayas realizado tus fotografías, para descargar las imágenes extrae la tarjeta de la cámara e introdúcela en tu computadora.

Una vez introducida verás en tu explorador de archivos las dos particiones: "boot" y "rootfs".

Las imágenes capturadas se almacenan en la partición "rootfs" en la ruta /home/pi/PicaPiKAP/media. Cópialas y pegalas en la memoria de tu computadora. Puedes eliminarlas al acabar de copiarlas para tener la tarjeta libre la próxima vez que la utilices.

