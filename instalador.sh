# !/bin/bash
#
# CÓMO UTILIZAR ESTE SCRIPT DE INSTALACIÓN
# Instala en la tarjeta microSD la última versión de RaspberryOS Lite 32bits usando Pi Imager que te permitirá instalarlo y configurar la red wifi (https://y.com.sb/watch?v=ntaXWS8Lk34).
# Enchufa la Raspberry Pi a un monitor y un teclado.
# Enciéndela.
# Entra en la carpeta de usuaria escribiendo "cd /home/pi"
# Descarga este archivo con "sudo wget https://gitlab.com/imvec/picapikap/-/raw/main/instalador.sh"
# Ejecuta el archivo con "sudo sh instalador.sh"
#
#
# HOW TO USE THIS INSTALLATION SCRIPT
# Install latest version of RaspberryOS Lite 32bits on the microSD card using Pi Imager that will allow you to install it and configure the wifi network (https://y.com.sb/watch?v=ntaXWS8Lk34)
# Plug the Raspberry Pi to a monitor and a keyboard.
# Boot it up.
# Go to your user directory typing "cd /home/pi"
# Get this installer file with "sudo wget https://gitlab.com/imvec/picapikap/-/raw/main/instalador.sh"
# Execute the file with "sudo sh instalador.sh"
#
#
 echo ""
 echo ""
 echo "" 
 echo ""
 echo ""
 echo ""
 echo "=============================="
 echo "   Preparando la instalación  "
 echo "=============================="
sleep 2
 echo ""
 echo ""
 echo "" 
 echo ""
 echo ""
 echo ""
 echo "==== Actualizando repositorios y sistema ===="
 echo ""
 echo "Actualización de los programas y de los repositorios de código"
sleep 3
 echo ""
apt update && upgrade
 echo ""
 echo ""
 echo "" 
 echo ""
 echo ""
 echo ""
 echo "==== Activando el módulo de cámara y los protocolos de conexión SPi, i2C y UART ===="
 echo ""
 echo "Se descarga un nuevo archivo de configuración de raspberry para su activación automática"
sleep 3
 echo ""
cd /boot
rm -rf config.txt
wget https://gitlab.com/imvec/picapikap/-/raw/main/config.txt
 echo ""
 echo ""
 echo "" 
 echo ""
 echo ""
 echo ""
 echo "==== Creando carpetas ===="
 echo ""
 echo "Se crea la carpera PicaPiKAP en la ruta /home/pi y en su interior dos carpetas más: media y scripts"
 echo "En la carpeta media se almacenan las imágenes y en la carpeta scripts el programa de disparo automático"
sleep 5
 echo ""
mkdir /home/pi/PicaPiKAP
mkdir /home/pi/PicaPiKAP/media
mkdir /home/pi/PicaPiKAP/scripts
 echo ""
 echo ""
 echo "" 
 echo ""
 echo ""
 echo ""
 echo "==== Adquiriendo el script de cámara y el registro de datos ===="
 echo ""
 echo "Se descarga el script que hará que la cámara se dispare automáticamente cada vez que arranque"
sleep 3
 echo ""
cd /home/pi/PicaPiKAP/scripts
wget https://gitlab.com/imvec/picapikap/-/raw/main/PicaPiKAP.py
chmod +x PicaPiKAP.py
 echo ""
 echo ""
 echo "" 
 echo ""
 echo ""
 echo ""
 echo "==== Actualizando repositorios ===="
 echo ""
sleep 2
 echo ""
apt update && upgrade
 echo ""
 echo ""
 echo "" 
 echo ""
 echo ""
 echo ""
 echo "==== Instalando python3-pip ===="
 echo ""
sleep 3
 echo ""
apt install python3-pip -y
 echo ""
 echo ""
 echo "" 
 echo ""
 echo ""
 echo ""
 echo "==== Instalando el módulo picamera ===="
 echo ""
sleep 3
 echo ""
pip3 install picamera
 echo ""
 echo ""
 echo "" 
 echo ""
 echo ""
 echo ""
 echo "==== Instalando el módulo datetime ===="
 echo ""
sleep 3
 echo ""
pip3 install datetime
 echo ""
 echo ""
 echo "" 
 echo ""
 echo ""
 echo ""
 echo "==== Instalando librería Adafruit circuit python BME280 ===="
 echo ""
 sleep 3
 echo ""
pip3 install adafruit-circuitpython-bme280
 echo ""
 echo ""
 echo "" 
 echo ""
 echo ""
 echo ""
 echo "==== Actualizando repositorios ===="
 echo ""
sleep 2
 echo ""
apt update && upgrade
 echo ""
 echo ""
 echo "" 
 echo ""
 echo ""
 echo ""
 echo "==== Instalando librerías y configurando el GPS ===="
 echo ""
 sleep 3
 echo ""
apt install gpsd gpsd-clients -y
sudo systemctl stop gpsd.socket
sudo systemctl disable gpsd.socket
pip3 install gpsd-py3
 echo ""
 echo ""
 echo "" 
 echo ""
 echo ""
 echo ""
 echo "==== Configurando el stream de video para la previsualización ===="
 echo ""
sleep 3
 echo ""
rpi-update -y
echo /opt/vc/lib/ | sudo tee /etc/ld.so.conf.d/vc.conf
ldconfig
curl https://www.linux-projects.org/listing/uv4l_repo/lpkey.asc | sudo apt-key add -
echo "deb https://www.linux-projects.org/listing/uv4l_repo/raspbian/stretch stretch main" | sudo tee /etc/apt/sources.list.d/uv4l.list
apt update && upgrade -y
apt install uv4l uv4l-raspicam uv4l-raspicam-extras uv4l-webrtc
bash -c "echo \"sudo service uv4l_raspicam start\" >> /etc/rc.local"
service uv4l_raspicam stop
service uv4l_raspicam start
 echo ""
 echo ""
 echo "" 
 echo ""
 echo ""
 echo ""
 echo "==== Actualizando repositorios por última vez ===="
 echo ""
sleep 2
 echo ""
apt update && upgrade
 echo ""
 echo ""
 echo "" 
 echo ""
 echo ""
 echo ""
 echo "==== Este instalador se autodestruirá en 7 segundos ===="
 echo ""
sleep 1
 echo "7"
sleep 1
 echo "6"
sleep 1
 echo "5"
sleep 1
 echo "4"
sleep 1
 echo "3"
sleep 1
 echo "2"
sleep 1
 echo "1"
sleep 1
 echo "0"
cd /home/pi
rm -rf instalador.sh
 echo ""
 echo ""
 echo ""
 echo ""
 echo ""
 echo ""
 echo ""
 echo ""
 echo ""
 echo ""
 echo "==============================================================================="
 echo "=                              SIGUIENTES PASOS                               ="
 echo "==============================================================================="
 echo "= Escribe: sudo crontab -e y añade estas dos lineas al final del archivo.     ="
 echo "= La cámara arrancará el GPS y comenzará a disparar al iniciarla:             ="
 echo "= @reboot sudo gpsd /dev/serial0 -F /var/run/gpsd.sock                        ="
 echo "= @reboot sleep 10 && sudo python3 /home/pi/PicaPiKAP/scripts/PicaPiKAP.py    ="
 echo "= Recomendamos retrasar 5 minutos (300 segundos) el inicio del disparo para   ="
 echo "= poder levantar la cometa y que el disparo se active en el aire. En ese caso ="
 echo "= debes añadir esta línea y no la anterior:                                   =" 
 echo "= @reboot sleep 300 && sudo python3 /home/pi/PicaPiKAP/scripts/PicaPiKAP.py   ="
 echo "=                                                                             ="
 echo "=              Reinicia la máquina con el comando: sudo reboot                ="
 echo "==============================================================================="
 echo ""
sleep 25
 echo ""
 echo "==============================================================================="
 echo "=                  ¿Dudas o problemas durante la instalación?                 ="
 echo "=             Ponte en contacto con nosotras en imvec@tutanota.com            ="
 echo "==============================================================================="
 echo ""
 echo ""
 echo ""
 echo ""
 echo ""
  
